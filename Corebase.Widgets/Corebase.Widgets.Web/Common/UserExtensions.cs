﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Corebase.Widgets.Web.Common
{
    public static class UserExtensions
    {
        public static IDictionary<string, string> GetClaims(this IPrincipal user, IEnumerable<string> claimNames)
        {
            IDictionary<string, string> claims = new Dictionary<string, string>();

            if (user.Identity is ClaimsIdentity identity)
            {
                claimNames.ToList().ForEach(cn =>
                {
                    string claim = identity.Claims.SingleOrDefault(c => c.Type == cn)?.Value;
                    if (!string.IsNullOrEmpty(claim))
                    {
                        claims.Add(cn, claim);
                    }
                });
            }

            return claims;
        }
    }
}