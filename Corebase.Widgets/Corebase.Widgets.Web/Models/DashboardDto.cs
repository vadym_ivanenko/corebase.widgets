﻿using System;
using System.Collections.Generic;

namespace Corebase.Widgets.Web.Models
{
    public class DashboardDto
    {

        public List<Widget> Widgets { get; set; }

        public class Widget
        {
            public int Id { get; set; }
            public Guid WidgetId { get; set; }
            public string Url { get; set; }
            public Position Position { get; set; }
            public Demension Demension { get; set; }
        }
    }
}