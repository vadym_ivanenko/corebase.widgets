﻿using System.ComponentModel.DataAnnotations;

namespace Corebase.Widgets.Web.Models
{
    public class LoginModel
    {
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
