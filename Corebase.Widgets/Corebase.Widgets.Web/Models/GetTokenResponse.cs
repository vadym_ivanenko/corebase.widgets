﻿namespace Corebase.Widgets.Web.Models
{
    public sealed class GetTokenResponse
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}