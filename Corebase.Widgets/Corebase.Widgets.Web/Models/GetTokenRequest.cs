﻿namespace Corebase.Widgets.Web.Models
{
    public sealed class GetTokenRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}