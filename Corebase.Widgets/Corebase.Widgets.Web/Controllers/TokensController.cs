﻿using System.Net;
using System.Threading.Tasks;
using Corebase.Widgets.Web.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Corebase.Widgets.Web.Controllers
{
    [Route("api/tokens")]
    [ApiController]
    [AllowAnonymous]
    public sealed class TokensController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TokensController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [ProducesResponseType(typeof(GetTokenResponse), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<ActionResult<GetTokenResponse>> PostAsync([FromBody]GetTokenRequest request)
        {
            TokenClient client = new TokenClient(
                address: $"{_configuration.GetValue<string>("IdentityServer:Authority")}/connect/token",
                clientId: "ro.client",
                clientSecret: "secret");

            IdentityModel.Client.TokenResponse response = await client.RequestResourceOwnerPasswordAsync(
                userName: request.UserName,
                password: request.Password,
                scope: "widgets.api").ConfigureAwait(false);

            if (response.IsError)
                return new StatusCodeResult((int)HttpStatusCode.Unauthorized);

            return new CreatedResult(string.Empty, new GetTokenResponse
            {
                AccessToken = response.AccessToken,
                ExpiresIn = response.ExpiresIn
            });
        }
    }
}