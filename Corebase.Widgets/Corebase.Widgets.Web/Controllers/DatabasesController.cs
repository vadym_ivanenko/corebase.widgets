﻿using System.Net;
using System.Threading.Tasks;
using Corebase.Widgets.Web.Domain;
using Corebase.Widgets.Web.Domain.Entities.Identity;
using Corebase.Widgets.Web.Domain.Seeder;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Corebase.Widgets.Web.Controllers
{
    [Route("system/db")]
    [ApiController]
    [AllowAnonymous]
    public sealed class DatabasesController : ControllerBase
    {
        private readonly IHostingEnvironment _env;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public DatabasesController(
            IHostingEnvironment env,
            ApplicationDbContext dbContext,
            UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {

            _env = env;
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost("migrate")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> MigrateAsync()
        {
            if (_env.IsDevelopment())
            {
                await _dbContext.Database.EnsureCreatedAsync();//.ConfigureAwait(false);
            }

            if (_env.IsStaging())
            {
                await _dbContext.Database.MigrateAsync().ConfigureAwait(false);
            }

            return new StatusCodeResult((int)HttpStatusCode.Created);
        }

        [HttpPost("seed")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> SeedAsync()
        {
            if (_env.IsDevelopment())
            {
                await _dbContext.Database.EnsureDeletedAsync().ConfigureAwait(false);
                await _dbContext.Database.EnsureCreatedAsync().ConfigureAwait(false);
            }

            if (_env.IsStaging())
            {
                await _dbContext.Database.MigrateAsync().ConfigureAwait(false);
            }

            await DataSeeder.SeedAsync(_dbContext, _userManager, _roleManager).ConfigureAwait(false);

            return new StatusCodeResult((int)HttpStatusCode.Created);
        }
    }
}