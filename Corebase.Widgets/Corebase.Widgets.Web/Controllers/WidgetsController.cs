﻿using System.Collections.Generic;
using Corebase.Widgets.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Corebase.Widgets.Web.Controllers
{
    [Route("widgets")]
    public class WidgetsController : Controller
    {
        [HttpGet]
        public IEnumerable<dynamic> Get(string token = null)
        {
            var demo = new WidgetMetadata
            {
                Id = "demo",
                Name = "Demo Widget",
                Description = "Some demo widget.",
                Configurator = new Configurator
                {
                    Url = "widgets/5ba70c7d-ac68-46ad-bd06-243b9372d368/configurator",
                    Scale = new Scale { Width = 300, Height = 300 }
                },
                Scales = new[] { new Scale { Width = 1, Height = 1 }, new Scale { Width = 2, Height = 1 } }
            };

            return new dynamic[] { demo };
        }
    }
}