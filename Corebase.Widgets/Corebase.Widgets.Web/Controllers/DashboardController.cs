﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Corebase.Widgets.Web.Domain;
using Corebase.Widgets.Web.Domain.Entities.Widget;
using Corebase.Widgets.Web.Models;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Corebase.Widgets.Web.Controllers
{
    [Route("api/dashboard")]
    [ApiController]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public DashboardController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("all")]
        public async Task<List<Dashboard>> GetAll()
        {
            return await _context.Dashboards.ToListAsync().ConfigureAwait(false);
        }

        [HttpGet]
        public async Task<ActionResult<DashboardDto>> Get()
        {
            List<DashboardDto.Widget> widgets = await _context.Dashboards.Where(w => w.UserId.ToString().Equals(User.Identity.GetSubjectId())).Select(dashboard => new DashboardDto.Widget()
            {
                Id = dashboard.Id,
                Position = new Position()
                {
                    X = dashboard.X,
                    Y = dashboard.Y
                },
                Demension = new Demension()
                {
                    H = dashboard.H,
                    W = dashboard.W
                },
                WidgetId = dashboard.WidgetId,
                Url = dashboard.Url
            }).ToListAsync().ConfigureAwait(false);

            DashboardDto dashboardDto = new DashboardDto()
            {
                Widgets = widgets
            };
            return dashboardDto;
        }

        [HttpPut]
        public async Task Put([FromBody]DashboardDto dto)
        {
            string userId = User.Identity.GetSubjectId();
            List<Dashboard> dashboardsToDelete = await _context.Dashboards.Where(d => d.UserId.ToString().Equals(userId)).ToListAsync().ConfigureAwait(false);
            _context.Dashboards.RemoveRange(dashboardsToDelete);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            foreach (DashboardDto.Widget widget in dto.Widgets)
            {
                _context.Dashboards.Add(new Dashboard()
                {
                    UserId = Guid.Parse(userId),
                    H = widget.Demension.H,
                    W = widget.Demension.W,
                    WidgetId = widget.WidgetId,
                    X = widget.Position.X,
                    Y = widget.Position.Y,
                    Url = widget.Url
                });
            }

            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task Post([FromBody] DashboardDto.Widget widget)
        {
            var userId = User.Identity.GetSubjectId();

            _context.Dashboards.Add(new Dashboard()
            {
                UserId = Guid.Parse(userId),
                H = widget.Demension.H,
                W = widget.Demension.W,
                WidgetId = widget.WidgetId,
                X = widget.Position.X,
                Y = widget.Position.Y,
                Url = widget.Url
            });

            await _context.SaveChangesAsync();
        }
    }
}