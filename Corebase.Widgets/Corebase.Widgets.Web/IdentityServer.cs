﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace Corebase.Widgets.Web
{
    internal static class IdentityServer
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources(string apiResource, string apiName)
        {

            return new List<ApiResource>
            {
                new ApiResource(apiResource, apiName)
                {
                    UserClaims = new List<string>
                    {
                        "email",
                        "role",
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId
                    }
                }
            };
        }

        public static IEnumerable<Client> GetClients(string clientId, string apiResource, string secret)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = clientId,
                    AccessTokenType = AccessTokenType.Jwt,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AccessTokenLifetime = 60 * 15,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    AbsoluteRefreshTokenLifetime = 60 * 60 * 2,
                    SlidingRefreshTokenLifetime = 60 * 60,
                    ClientSecrets = new List<Secret>
                    {
                        new Secret(secret.Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                        apiResource,
                        "email",
                        "role",
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId
                    }
                }
            };
        }
    }
}