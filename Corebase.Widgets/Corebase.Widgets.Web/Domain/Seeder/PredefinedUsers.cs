﻿using Corebase.Widgets.Web.Domain.Entities.Identity;

namespace Corebase.Widgets.Web.Domain.Seeder
{
    public static class PredefinedUsers
    {
        public static User User1 = new User
        {
            UserName = "test1",
            Email = "test1@mailinator.com",
            EmailConfirmed = true
        };

        public static User User2 = new User
        {
            UserName = "test2",
            Email = "test2@mailinator.com",
            EmailConfirmed = true
        };

        public static User User3 = new User
        {
            UserName = "test3",
            Email = "test3@mailinator.com",
            EmailConfirmed = true
        };
    }
}