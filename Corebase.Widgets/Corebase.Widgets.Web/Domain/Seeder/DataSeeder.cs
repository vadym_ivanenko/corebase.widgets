﻿using System.Threading.Tasks;
using Corebase.Widgets.Web.Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;

namespace Corebase.Widgets.Web.Domain.Seeder
{   
    public static class DataSeeder
    {
        public static async Task SeedAsync(ApplicationDbContext dbContext, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            const string password = "cbPassw0rD!";
            Role role1 = new Role
            {
                Name = "Role1"
            };

            Role role2 = new Role
            {
                Name = "Role2"
            };

            Role role3 = new Role
            {
                Name = "Role3"
            };

            await roleManager.CreateAsync(role1).ConfigureAwait(false);
            await roleManager.CreateAsync(role2).ConfigureAwait(false);
            await roleManager.CreateAsync(role3).ConfigureAwait(false);

            User user1 = PredefinedUsers.User1;
            await userManager.CreateAsync(user1, password).ConfigureAwait(false);
            
            await userManager.AddToRoleAsync(user1, role1.Name).ConfigureAwait(false);

            User user2 = PredefinedUsers.User2;
            await userManager.CreateAsync(user2, password).ConfigureAwait(false);

            await userManager.AddToRoleAsync(user2, role1.Name).ConfigureAwait(false);
            await userManager.AddToRoleAsync(user2, role2.Name).ConfigureAwait(false);

            User user3 = PredefinedUsers.User3;
            await userManager.CreateAsync(user3, password).ConfigureAwait(false);

            await userManager.AddToRoleAsync(user3, role1.Name).ConfigureAwait(false);
            await userManager.AddToRoleAsync(user3, role2.Name).ConfigureAwait(false);
            await userManager.AddToRoleAsync(user3, role3.Name).ConfigureAwait(false);

            await dbContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}