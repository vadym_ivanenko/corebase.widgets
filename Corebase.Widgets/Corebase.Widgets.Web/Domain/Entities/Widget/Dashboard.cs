﻿using System;
using Corebase.Widgets.Web.Domain.Entities.Identity;

namespace Corebase.Widgets.Web.Domain.Entities.Widget
{
    public class Dashboard
    {
        public int Id { get; set; }
        public Guid WidgetId { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int W { get; set; }
        public int H { get; set; }
        public string Url { get; set; }
    }
}