using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Corebase.Widgets.Web.Domain.Entities.Identity
{
    public sealed class Role : IdentityRole<Guid>
    {
        public Role()
        {
            Users = new List<IdentityUserRole<Guid>>();
            Claims = new List<IdentityRoleClaim<Guid>>();
        }

        public ICollection<IdentityUserRole<Guid>> Users { get; set; }
        public ICollection<IdentityRoleClaim<Guid>> Claims { get; set; }
    }
}