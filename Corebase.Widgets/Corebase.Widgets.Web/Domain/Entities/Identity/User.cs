﻿using System;
using System.Collections.Generic;
using System.Linq;
using Corebase.Widgets.Web.Domain.Entities.Widget;
using Microsoft.AspNetCore.Identity;

namespace Corebase.Widgets.Web.Domain.Entities.Identity
{
    public sealed class User : IdentityUser<Guid>
    {
        public User()
        {
            Roles = Enumerable.Empty<IdentityUserRole<Guid>>().ToList();
            Claims = Enumerable.Empty<IdentityUserClaim<Guid>>().ToList();
            Widgets = Enumerable.Empty<Dashboard>().ToList();
        }

        public ICollection<IdentityUserRole<Guid>> Roles { get; set; }
        public ICollection<IdentityUserClaim<Guid>> Claims { get; set; }
        public ICollection<Dashboard> Widgets { get; set; }
    }
}