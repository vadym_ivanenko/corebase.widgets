﻿using System;
using System.Linq;
using System.Reflection;
using Corebase.Widgets.Web.Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;

namespace Corebase.Widgets.Web.Domain
{
    internal static class ModelBuilderExtensions
    {
        public static ModelBuilder OverrideAspNetIdentityModelCreating(this ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<User>()
                .HasMany(e => e.Claims)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Role>()
                .HasMany(e => e.Claims)
                .WithOne()
                .HasForeignKey(e => e.RoleId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            return builder;
        }

        public static ModelBuilder ApplyEntityConfigurations(this ModelBuilder builder)
        {
            Type interfaceType = typeof(IEntityTypeConfiguration<>);
            MethodInfo applyGenericMethod = builder.GetType().GetMethods()
                .FirstOrDefault(m => m.Name.Equals("ApplyConfiguration")
                                     && m.GetParameters().Any(p => p.ParameterType.IsGenericType && p.ParameterType.GetGenericTypeDefinition() == interfaceType));

            if (applyGenericMethod != null)
            {
                foreach (Type type in typeof(ModelBuilderExtensions).Assembly.GetTypes()
                    .Where(c => c.IsClass && !c.IsAbstract && !c.ContainsGenericParameters))
                {
                    foreach (Type iface in type.GetInterfaces()
                        .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType))
                    {
                        MethodInfo applyConcreteMethod =
                            applyGenericMethod.MakeGenericMethod(iface.GenericTypeArguments.FirstOrDefault());
                        applyConcreteMethod.Invoke(builder, new[] { Activator.CreateInstance(type) });
                    }
                }
            }

            return builder;
        }
    }
}