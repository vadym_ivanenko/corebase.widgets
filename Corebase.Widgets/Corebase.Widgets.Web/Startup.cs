﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Corebase.Widgets.Integration;
using Corebase.Widgets.Web.Domain;
using Corebase.Widgets.Web.Domain.Entities.Identity;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Corebase.Widgets.Web
{
    public class Startup
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            //services.AddWidgets();

            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(
                    Configuration.GetConnectionString("CoreBase")));

            services.AddIdentity<User, Role>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                    options.Lockout.MaxFailedAccessAttempts = 3;
                    options.SignIn.RequireConfirmedEmail = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddCors();

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            ConfigureIdentityServer(services, Configuration, _hostingEnvironment);
            ConfigureAuthentication(services, Configuration);


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment() || env.IsStaging())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseHttpsRedirection();

            //app.UseWidgets($"{env.ContentRootPath}/Widgets");

            app.UseCors(c => c.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials());
            app.UseMvc();
        }

        private static void ConfigureIdentityServer(IServiceCollection services, IConfiguration configuration, IHostingEnvironment environment)
        {
            IIdentityServerBuilder builder = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
            })
                .AddInMemoryIdentityResources(IdentityServer.GetIdentityResources())
                .AddInMemoryApiResources(IdentityServer.GetApiResources(
                    "widgets.api",
                    "Widgets API"))
                .AddInMemoryClients(IdentityServer.GetClients("ro.client", "widgets.api", "secret"))
                .AddAspNetIdentity<User>();
            builder.AddDeveloperSigningCredential();
        }

        private static void ConfigureAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(o =>
            {
                o.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                o.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
            })

            .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = configuration.GetValue<string>("IdentityServer:Authority");
                    options.RequireHttpsMetadata = true;
                    options.ApiName = "Widgets API";
                    options.LegacyAudienceValidation = true;

                });
        }
    }
}