﻿using System;

namespace Corebase.Widgets.Demo.Models
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public Department Department { get; set; }
        public Branch Branch { get; set; }
    }

    public class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }

    public class Branch
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
    }
}
