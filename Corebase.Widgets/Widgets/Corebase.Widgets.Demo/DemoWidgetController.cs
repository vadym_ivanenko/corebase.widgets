﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Corebase.Widgets.Demo.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;

namespace Corebase.Widgets.Demo
{
    //[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    [Route(Route)]
    public class DemoWidgetController : WidgetController
    {
        const string WidgetId = "5ba70c7d-ac68-46ad-bd06-243b9372d368";
        const string Route = "widgets/5ba70c7d-ac68-46ad-bd06-243b9372d368";

        private readonly IDemoWidgetService _demoWidgetService;

        public DemoWidgetController()
        {
            _demoWidgetService = new DemoWidgetService();
        }

        public async override Task<IActionResult> Get(string token, IDictionary<string, string> args)
        {
            var employees = await _demoWidgetService.GetWidgetData(args);

            var mode = "Release";
#if DEBUG
            mode = "Debug";
#endif
            var view = View($"bin/{mode}/netcoreapp2.1/Views/Index.cshtml", new JsonResult(employees));

            return await Task.FromResult(view);
        }

        public async override Task<IActionResult> GetConfigurator()
        {
            var mode = "Release";
#if DEBUG
            mode = "Debug";
#endif
            var view = View($"bin/{mode}/netcoreapp2.1/Views/Configurator.cshtml");
            return await Task.FromResult(view);
        }

        public async override Task<WidgetMetadata> GetWidgetMetadata()
        {
            var metadata = new WidgetMetadata
            {
                Id = WidgetId,
                Name = "Demo Widget",
                Description = "Some demo widget.",
                Configurator = new Configurator
                {
                    Url = $"{Route}/configurator",
                    Scale = new Scale { Width = 300, Height = 300 }
                },
                Scales = new[] { new Scale { Width = 1, Height = 1 }, new Scale { Width = 2, Height = 1 } }
            };
            return await Task.FromResult(metadata);
        }
    }
}