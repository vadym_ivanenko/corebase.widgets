﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Corebase.Widgets.Demo.Models;

namespace Corebase.Widgets.Demo.Services
{
    public class DemoWidgetService : IDemoWidgetService
    {
        public async Task<List<Employee>> GetWidgetData(IDictionary<string, string> args)
        {
            var employees = await getEmployeesFromFile();
            foreach (var arg in args)
            {
                if (arg.Key == "branch" && !string.IsNullOrEmpty(arg.Value))
                {
                    employees = employees.Where(x => x.Branch.BranchId == int.Parse(arg.Value));
                }
                if (arg.Key == "department" && !string.IsNullOrEmpty(arg.Value))
                    employees = employees.Where(x => x.Department.DepartmentId == int.Parse(arg.Value));
            }
            return employees.ToList();
        }

        private async Task<IEnumerable<Employee>> getEmployeesFromFile()
        {
            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directory = System.IO.Path.GetDirectoryName(location);
            var fileName = "employees.csv";
            var path = Path.Combine(directory, "data", fileName);

            if (File.Exists(path))
            {
                    var lines = await File.ReadAllLinesAsync(path);
                    return (from line in lines
                        let columns = line.Split(',')
                        select new Employee()
                        {
                            FirstName = columns[0],
                            LastName = columns[1],
                            Birthday = DateTime.Parse(columns[2]),
                            Department = new Department()
                            {
                                DepartmentName = columns[3],
                                DepartmentId = int.Parse(columns[4])
                            },
                            Branch = new Branch()
                            {
                                BranchName = columns[5],
                                BranchId = int.Parse(columns[6])
                            }
                        });
            }
            throw new FileNotFoundException("", path);
        }
    }
}
