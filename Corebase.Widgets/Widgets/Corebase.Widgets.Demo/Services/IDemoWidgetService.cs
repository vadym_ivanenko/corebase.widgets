﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Corebase.Widgets.Demo.Models;

namespace Corebase.Widgets.Demo.Services
{
    public interface IDemoWidgetService
    {
        Task<List<Employee>> GetWidgetData(IDictionary<string, string> args);
    }
}
