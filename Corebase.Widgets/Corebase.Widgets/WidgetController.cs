﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Corebase.Widgets
{
    public abstract class WidgetController : Controller
    {
        public abstract Task<IActionResult> Get(string token, IDictionary<string, string> args);

        [HttpGet("meta")]
        public abstract Task<WidgetMetadata> GetWidgetMetadata();

        [HttpGet("configurator")]
        public abstract Task<IActionResult> GetConfigurator();
    }
}