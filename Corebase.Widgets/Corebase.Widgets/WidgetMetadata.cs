﻿namespace Corebase.Widgets
{
    public class WidgetMetadata
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Configurator Configurator { get; set; }

        public Scale[] Scales { get; set; } = new Scale[0];

        public WidgetMetadata() {}

        public WidgetMetadata(string id, string name, string description, Configurator configurator, Scale[] scales)
        {
            Id = id;
            Name = name;
            Description = description;
            Configurator = configurator;
            Scales = scales;
        }
    }

    public class Configurator
    {
        public string Url { get; set; }

        public Scale Scale { get; set; }
    }

    public class Scale
    {
        public int Width { get; set; }

        public int Height { get; set; }
    }
}