﻿using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Corebase.Widgets.Integration
{
    public static class Extensions
    {
        private static FileSystemWatcher FileSystemWatcher;

        public static IServiceCollection AddWidgets(this IServiceCollection services)
        {
            services.AddSingleton<IActionDescriptorChangeProvider>(CustomActionDescriptorChangeProvider.Instance);
            services.AddSingleton(CustomActionDescriptorChangeProvider.Instance);

            return services;
        }

        public static IApplicationBuilder UseWidgets(this IApplicationBuilder applicationBuilder, string widgetsDirectory)
        {
            LoadWidgets(applicationBuilder, widgetsDirectory);

            FileSystemWatcher = new FileSystemWatcher(widgetsDirectory);
            FileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
            FileSystemWatcher.Filter = "*.*";
            FileSystemWatcher.Changed += (object sender, FileSystemEventArgs e) => LoadWidgets(applicationBuilder, widgetsDirectory);
            FileSystemWatcher.EnableRaisingEvents = true;

            return applicationBuilder;
        }

        private static void LoadWidgets(IApplicationBuilder applicationBuilder, string widgetsDirectory)
        {
            var assemblies = Directory.GetFiles(widgetsDirectory, "*.dll", SearchOption.AllDirectories)
                    .Select(Assembly.LoadFile)
                    .ToList();
            if (assemblies.Any())
            {
                foreach (var assembly in assemblies)
                {
                    var assemblyPart = new AssemblyPart(assembly);
                    var applicationPartManager = applicationBuilder.ApplicationServices.GetService<ApplicationPartManager>();
                    if (!applicationPartManager.ApplicationParts.Contains(assemblyPart))
                    {
                        applicationPartManager.ApplicationParts.Add(assemblyPart);
                    }
                }

                CustomActionDescriptorChangeProvider.Instance.HasChanged = true;
                CustomActionDescriptorChangeProvider.Instance.TokenSource.Cancel();
            }
        }
    }
}