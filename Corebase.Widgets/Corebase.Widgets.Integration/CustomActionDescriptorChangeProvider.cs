﻿using System.Threading;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Primitives;

namespace Corebase.Widgets.Integration
{
    public class CustomActionDescriptorChangeProvider : IActionDescriptorChangeProvider
    {
        public static CustomActionDescriptorChangeProvider Instance { get; } = new CustomActionDescriptorChangeProvider();

        public CancellationTokenSource TokenSource { get; private set; }

        public bool HasChanged { get; set; }

        public IChangeToken GetChangeToken()
        {
            TokenSource = new CancellationTokenSource();
            return new CancellationChangeToken(TokenSource.Token);
        }
    }
}